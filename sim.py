import logging

def run(df,
      solar = 1, wind = 1,
      max_charge_mw = 1_000_000, max_discharge_mw = 1_000_000, max_battery = 1_000,
      title = 'Simulation',
      epoch_minutes = 5,
      starting_battery_percent = 50,
      log_level = 'INFO',
     ):
  df['SIM_SOLAR'] = df['SOLAR'] * solar
  df['SIM_WIND'] = df['WIND'] * wind
  df['BATTERY_STATE'] = 0.0
  df['BATTERY_CHARGE_RATE'] = 0.0
  df['DEFICIT'] = 0.0
  df['BALANCE'] = 0.0 # sum of deficit and surplus
  log = logging.getLogger()
  log.setLevel(log_level)

  battery_state = starting_battery_percent * max_battery / 100
  e = epoch_minutes / 60
  # battery rates per epoch minutes
  mc = max_charge_mw * e # MWh
  md = max_discharge_mw * e # MWh
  log.info(f'Epoch is {epoch_minutes:.1f} minutes')
  log.info(f'''{title}
  Battery:
    Usable capacity {max_battery:.1f}MWh
    Max Charging/Discharging {max_discharge_mw:.1f}/{max_charge_mw:.1f}MW ({mc:.1f}/{md:.1f}MWh/epoch)
  Generation:
    Wind: x {wind}
    Solar: x {solar}
    ''')
  for i,r in df.iterrows():
      gen = r.SIM_SOLAR + r.SIM_WIND
      gap = r.DEMAND - gen
      if gap != gap: # we have NaN
        log.error(f'NaN at {i} {r.SIM_SOLAR} + {r.SIM_WIND}')
        gap = 0
      gap_MWh = gap * e
      log.debug(f'{i} generation: {gen:.1f}MW demand: {r.DEMAND:.1f}MW gap: {gap:.1f}MW ({gap_MWh:.1f}MWh)')
      if gap < 0: # deal with excess power
          # fill battery as fast as it can until its full
          charge = min(-gap_MWh, mc, max_battery - battery_state)
      else:
          # discharge required MWh bounded by discharge limit and battery state
          charge = -min(gap_MWh, md, battery_state)
      battery_state += charge
      df.at[i,'BATTERY_CHARGE_RATE'] = charge / e
      df.at[i,'BATTERY_STATE'] = battery_state
      balance = gap + charge / e
      df.at[i,'BALANCE'] = balance + 0.01
      df.at[i,'DEFICIT'] = max(0, balance)
      log.debug(f'Balance: {r.BALANCE:.1f} charge {charge:.1f}MWh battery_state: {battery_state:.1f}MWh')
  return df
