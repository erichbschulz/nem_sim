import logging
from opennempy import web_api

regions = ['nsw1', 'qld1', 'sa1','tas1','vic1']

def getdata(d1, d2, regions,
      data_dir = 'data',
      logger = False, log_level = logging.INFO,
      five_minutely = True
    ):
    results = {}
    print('data_dir:', data_dir)
    web_api.data_dir = data_dir
    if logger:
      web_api.log.addHandler(logger)
    web_api.log.setLevel(log_level)
    print('data_dir:', data_dir)
    for region in regions:
        print('getting',d1, d2, region)
        df_5, df_30 = web_api.load_data(
            d1 = d1, # datetime.datetime(2018,3,4),
            d2 = d2, # datetime.datetime(2018,4,19),
            region=region)
        print('got df_5, df_30', len(df_5), len(df_30), 'rows')
        results[region] = df_5 if five_minutely else df_30
    return results
