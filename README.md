# NEM Simulator

This is a toolbox to run "What if?" scenarios on the NEM dataset.

# Installation

You'll need python >= 3.6 and for most fun use JupyterLab or Jupyter (get the
    [Anaconda platform](https://www.anaconda.com/download) for an easy life).

You may need to `pip install` missing modules. If you do please open an issue.

```
git clone git@bitbucket.org:erichbschulz/nem_sim.git
cd nem_sim
git clone https://github.com/opennem/opennempy
cp opennempy/config_sample.ini opennempy/config.ini
```

# Use
Simplest use is to open up the note book and step through the code.

# Architecture

The opennempy module provides an interface to the OpenNEM project data. On first call it will pull required data and locally cache for you.

Ideally reusable code will live in discrete functions leaving the notebooks to explore and present.
So there will probably be a constant gradual emptying of code out of the notebook.

# Roadmap

There's a few question running simulation can hopefully answer:

* What is the optimal mix of energy generators?
* How much storage is required?
* How much will it all cost?


# Acknowledgement

This project is aided, inspired and indebted to
[yestiseye](https://github.com/yestiseye/whtr) and the
[OpenNem team](https://github.com/opennem/opennempy), and anyone else who ever submits a bug report or PR.

# Licence

Whatever. Don't blame me. That is all.
